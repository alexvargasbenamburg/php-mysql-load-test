FROM trafex/alpine-nginx-php7
# copy in code
ADD ./index.php /var/www/html/
ADD ./Database.php /var/www/html/

ENV DB_MASTER=mysql
ENV DB_SLAVE=mysql
ENV DB_PASS=a1b2c3d4e5

EXPOSE 8080

WORKDIR "/var/www/html"
