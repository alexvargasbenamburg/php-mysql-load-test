grant all privileges on load_test.* to 'load_test'@'%' identified by 'a1b2c3d4e5';
create database load_test;
CREATE TABLE load_test.data (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `client` varchar(16) NOT NULL,
  `appHost` varchar(45) NOT NULL,
  `dbHost` varchar(45) NOT NULL,
  `time` float DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
