<?php
require_once('Database.php');
$connW = new Database(getenv ( "DB_MASTER"), 'load_test', getenv ( "DB_PASS" ), 'load_test');
$connR = new Database(getenv ( "DB_SLAVE" ), 'load_test', getenv ( "DB_PASS" ), 'load_test');


// Realizar una consulta MySQL
$query = 'select count(*) as cantidad, @@hostname as host from load_test.data';
$results = $connR->executeQuery($query);

if ($results->num_rows > 0) {
    $row = $results->fetch_array();
    $insert = $connW->executeQuery("INSERT INTO load_test.data(client,appHost,dbHost) VALUES ('?', '?', '?')", array($_SERVER["HTTP_X_FORWARDED_FOR"], gethostname(), $row['host'])) or die(var_dump($connW->connection->error_list));
    $id = $connW->connection->insert_id;
    $executionTime = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"];
    $sqlUpdate = "update load_test.data set time = " . $executionTime . " where id=" . $id . ";";
    $connW->executeQuery($sqlUpdate);
}
echo microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"];

